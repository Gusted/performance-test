#!/bin/sh
for n in {1..1000}; do
    mkdir file$( printf %03d "$n")
    dd if=/dev/urandom of=file$( printf %03d "$n" ).bin bs=1 count=$(( RANDOM + 1024 ))
    dd if=/dev/urandom of=file$( printf %03d/%03d "$n" "$n" ).bin bs=1 count=$(( RANDOM + 1024 ))
done
